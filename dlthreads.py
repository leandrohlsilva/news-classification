"""
Module that sets up download processes

2011-01-03
"""

import sys, socket, urllib.request, urllib.error, urllib.parse
socket.setdefaulttimeout(20)

from multiprocessing import Queue, Process
from queue import Empty, Full
import feedparser

def worker():
	while True:
		try: 
			d = urlq.get()
		except: pass
		try:
			result = get_content_unicode(d)
		except (urllib.error.HTTPError, AttributeError) as e:
			#print("Error: {0} - {1}".format(d, e))
			result = "failed"
		urlr.put((d, result))

def get_content_unicode(url):
	"Get the URL content and convert to Unicode."
	ugen = urllib.request.urlopen(url)
	rawhtml = ugen.read()
	return rawhtml
	
def start():
	global num_worker_threads, urlq, urlr, new_data, program_run, ti
	num_w = 1		# number of download processes
	urlq = Queue()		# query queue
	urlr = Queue()		# result queue

	for i in range(num_w):
		ti = Process(target=worker)
		ti.daemon = True
		ti.start()
