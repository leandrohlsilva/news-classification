#!/usr/bin/env python

"""
NewsFeed

"""

__author__    = "Leandro Lourenco (leandro@parallaxis.com.br)"

import sys
assert sys.version >= '3.3', "This program does not work with older versions of Python.\
 Please install Python 3.3 or later."

import os, sys, time, string, re, webbrowser, pickle, signal, socket, urllib.request, urllib.error, urllib.parse, difflib
socket.setdefaulttimeout(20)
from hashlib import md5
from multiprocessing import Queue
from queue import Empty, Full
from html.entities import html5 as entdic
from html.parser import HTMLParser
import pdb
import feedparser, rssfinder, dlthreads
import xmltodict
from boto3 import session
import xml


newsfeeds = []

new_data = {}		# global hash for freshly-downloaded feed content

class ContentItem:
	"A channel content class."
	def __init__(s, title, descr, link, date, fromfeed = "", enclosure = []):
		s.title     = title
		s.descr     = descr
		s.link      = link
		s.date      = date
		s.fromfeed  = fromfeed
		s.enclosure = enclosure
		s.unread    = True

	def get_descr(s):
		fieldValue = re.sub("<.*?>", "", s.descr)
		fieldValue = htmlrender(stripcontrol(fieldValue)).strip()
		if fieldValue == fieldValue.upper(): fieldValue = title_caps(fieldValue)
		fieldValue = [x for x in fieldValue.split() if x not in ('{/', '{[', ']}')]
		return ' '.join(fieldValue)

	def get_date(s):
		return s.date

	def get_enclosure(s):
		return s.enclosure

	def get_link(s):
		return s.link

	def get_fromfeed(s):
		return s.fromfeed

	def get_p_title(s):
		"Return textbox title of item."
		fieldValue = re.sub("<.*?>", "", s.title)
		fieldValue = htmlrender(stripcontrol(fieldValue)).strip()
		if fieldValue == fieldValue.upper(): fieldValue = title_caps(fieldValue)
		fieldValue = [x for x in fieldValue.split() if x not in ('{/', '{[', ']}')]
		return ' '.join(fieldValue)

	def get_title(s):
		"Return listview title of item"
		title = s.get_p_title()
		return title

	def get_s_title(s):
		"Return title of item as seen in search results."
		return s.get_title() + " [" + s.fromfeed + "]"

	def get_date(s):
		"Return the date of the item."
		return s.date

	def has_enclosure(s):
		"Does the item have an enclosure attached?"
		try: return s.enclosure != []
		except: return False

	def print_values(s):
		print('title = {0}'.format(s.get_title()))
		print('descr = {0}'.format(s.get_descr()))
		print('date = {0}'.format(s.get_date()))
		print('link = {0}'.format(s.get_link()))
		print('fromfeed = {0}'.format(s.get_fromfeed()))

	def is_valid(s):
		return (s.get_title() and s.get_descr() and s.get_date() and s.get_link() and s.get_fromfeed())

class NewsWire:
	"A channel class that stores its content in s.contents"
	def __init__(s, url = "", name = "", homeurl = "",
			refresh = 5, expire = 60*30):
		if url == "": raise IOError
		s.url        = url
		s.name       = name
		s.descr      = ""
		s.homeurl    = homeurl
		s.refresh    = refresh
		s.expire     = expire
		s.content    = []
		s.headlines  = {}
		s.u_time     = approx_time()		# Time of last update
		s.failed     = False
		s.lastresult = None		# Store last result for conditional GET

	def get_name(s):
		"Return newsfeed name, optionally with number of unread items."
		if s.failed or not s.content: return "  [" + s.name + "]"
		num_unread = s.get_unread()
		if num_unread: return s.name + " (" + str(num_unread) + ")"
		else: return s.name

	def get_unread(s):
		"Return number of unread items in newsfeed."
		return len([x for x in s.content if x.unread])

	def read(s):
		"Return list of unread items in newsfeed."
		returned_list = [x for x in s.content if x.unread]
		for i in s.content:
			i.unread = False

		return returned_list

	def _get_atom(s, l):
		"Get HTML or text content from Atom feed."
		if not l: return ""
		res = ""
		for x in l:
			t = x.get("type", "").lower()
			if "html" in t or "text" in t or not t:
				res = x.get("value", "")
		return res

	def _get_diff(s, a, b, only_added = False):
		"Get a difference between the texts a and b."
		x = difflib.ndiff(a.splitlines(1), b.splitlines(1))
		if only_added: x = [y[2:] for y in x if y[0] == '+' and len(y) > 5]
		return '<br>'.join(x)

	def url_is_webpage(s):
		"Does the feed URL point to text instead of XML?"
		if s.url == 'http://':
			return True
		try: s.is_webpage
		except: s.update_content_type()
		return s.is_webpage

	def _get_content_type(s):
		"Get the URL content type, differentiate between HTML and XML."
		if '.htm' in s.url.split('/')[-1]:
			return 'text/html'
		try:
			ugen = urllib.request.urlopen(s.url)
			th = ugen.info().typeheader

			# some servers mistakenly report "text/html" for XML files,
			#   so check the actual page content:
			if 'xml' not in th:
				rawhtml = ugen.read()
				if '<?xml' in rawhtml or '<rss' in rawhtml: return 'text/xml'
				elif '<html' in rawhtml.lower(): return 'text/html'
				else: return th
			else: return th
		except: return '(None)'

	def get_news(s, refresh = False):
		"Get news items from the Web and decode to instance variables."
		result = {}
		newcontent = []

		if s.homeurl == "" or s.homeurl == "http://":
			try: s.homeurl = re.compile("http://([^/]+)/").match(s.url).expand("\g<0>")
			except: pass

		if (s.content == [] or s.failed or refresh):
			# For backwards compatibility:
			try: s.lastresult
			except AttributeError:
				s.lastresult = {}
				s.lastresult['etag'] = ''
				s.lastresult['modified'] = ''
			try: s.webpage
			except AttributeError: s.webpage = ''

			#pdb.set_trace()

			try:
				rawhtml = new_data.get(s.url)
				if rawhtml == "failed": raise TypeError
			except:
				s.failed = True
				return 0
			else: s.failed = False
			if not rawhtml: return 0

			try:
				result = xmltodict.parse(rawhtml)
			except xml.parsers.expat.ExpatError:
				s.failed = True
				return 0
			if (not result.get('rss')):
				s.failed = True
				return 0
			s.title  = result['rss']['channel'].get('title', "").strip()
			if s.name[0] == '?' and s.title: s.name = s.title
			s.date   = (result['rss']['channel'].get('modified',
				time.strftime("%Y-%m-%d %H:%M", time.localtime())).strip())
			try:
				s.descr  = ( result['rss']['channel'].get('description', "").strip() or
					result['rss']['channel'].get('summary', s.descr).strip() )
			except: s.descr = ""

			for item in result['rss']['channel']['item']:
				# Each item is a dictionary mapping properties to values
				
				descr = ( item.get('description', "") or
					     s._get_atom(item.get('content', "")) or
					     item.get('summary', "") or "Sem descrição")
				title = item.get('title', re.sub("<.*?>", "", descr))
				hash = gethash(title, descr)
				if hash not in list(s.headlines.keys()):
					s.headlines[hash] = s.u_time
					link  = item.get('link', "(none)")
					date  = item.get('modified', s.date)
					enc   = item.get('enclosures', [])
					newcontent.append(ContentItem(title, descr, link,
							date, fromfeed = s.name, enclosure = enc))
			s.content = newcontent + s.content
			for i in list(s.headlines.keys()):
				if (time.time() - s.headlines[i]) > s.expire:
					for j in range(len(s.content) - 1, -1, -1):
						if (gethash(s.content[j].title, s.content[j].descr) == i):
							del s.content[j]
							s.headlines[i] = None
			for i in list(s.headlines.keys()):
				if s.headlines[i] == None: del s.headlines[i]

		return len(newcontent)

def is_existent_feed(newsfeeds, i):
	for n in newsfeeds:
		if (n.get_name() == i['name']['S']):
			return True
	return False


def add_feeds(obj):
	"Accept a list of tuples and add them to the global newsfeed pool."
	global newsfeeds

	for i in obj:
		try:
			if (not is_existent_feed(newsfeeds, i) and 'link' in i):
				newsfeeds.append(NewsWire(i['link']['S'], name=i['name']['S']))
		except IOError:
			print("Error: Could not find a suitable newsfeed.")

class MyHTMLParser(HTMLParser):
	"Parser for htmlrender function."
	def __init__(s):
		s.out = ''
		super().__init__(s)
	def handle_starttag(s, t, a):
		if t == 'a':
			for at in a:
				if at[0] == 'href':
					s.out += ' {[ ' + at[1] + ' | '
		if t == 'br':
			s.out += ' {/ '
		if t == 'b':
			s.out += '*'
		if t == 'strong':
			s.out += '**'
		if t == 'u':
			s.out += '_'
		if t == 'i' or t == 'em':
			s.out += '~'
		if t == 'img':
			s.out += '[Image]'
		if t == 'blockquote':
			s.out += ' {/ {/ '
		if t == 'li':
			s.out += ' {/ *'
	def handle_endtag(s, t):
		if t == 'a':
			s.out += ' ]} '
		if t == 'p':
			s.out += ' {/ {/ '
		if t == 'b':
			s.out += '*'
		if t == 'strong':
			s.out += '**'
		if t == 'u':
			s.out += '_'
		if t == 'i' or t == 'em':
			s.out += '~'
		if t == 'li':
			s.out += ' {/ '
	def handle_data(s, d):
		s.out += d
	def handle_entityref(s, n):
		s.out += entdic.get(n + ';', '&' + n)
	def handle_charref(s, c):
		if c.startswith('x'):
			s.out += chr(int(c[1:], 16))
		else:
			s.out += chr(int(c))

def htmlrender(t):
	"Transform HTML markup to printable text."
	parser = MyHTMLParser()
	parser.feed(t)
	return parser.out

def stripcontrol(t, keep_newlines = False):
	"Strip control characters from t."
	if keep_newlines:
		subs = ('\r', ''),
	else:
		subs = ('\r', ''), ('\n', ' ')
	for i in subs: t = t.replace(i[0], i[1])
	return t

def approx_time():
	"Return an approximate timestamp, so that feeds and stories stay in sync."
	return 10. * int(time.time() / 10)

def gethash(*args):
	"Compute the MD5 hash of the arguments concatenated together."
	h = md5()
	for i in args:
		if type(i) != type(""): h.update(i)
		else: h.update(i.encode("utf-8", "replace"))
	return h.hexdigest()

def title_caps(t):
	"Do a decent title capitalization."
	words = ["a", "an", "the", "some", "and", "but", "of",
				"on", "or", "nor", "for", "with", "to", "at"]
	t = t.title()
	t = t.replace("'S", "'s")
	for i in words: t = t.replace(" %s " % i.title(), " %s " % i)
	return t

def get_sources(dynamodb):
	result = dynamodb.scan(table_name='news_source')
	return result['Items']

def saveNewsEntry(c, dynamodb):
	news_insert = {'link': {'S': c.get_link()}, \
	'source': {'S': c.get_fromfeed()}, \
	'title': {'S': c.get_title()}, \
	'description': {'S': c.get_descr()}, \
	'date': {'N': str(time.mktime(time.strptime(c.get_date(), '%Y-%m-%d %H:%M')))} \
	}

	#print(news_insert)
	dynamodb.put_item(table_name='news', item=news_insert)
	time.sleep(0.2)


def main():
	"Main Program. Start either textual or graphical interface."

	dynamodb = session.connect_to('dynamodb', region_name='sa-east-1')

	dlthreads.start()
	sources = get_sources(dynamodb)
	for i in sources:
		if 'link' in i: #Research doesn't have link attribute
			dlthreads.urlq.put_nowait(i['link']['S'])

	time.sleep(1)

	add_feeds(sources)

	start = time.time()

	while (True):
		if (time.time() - start) > 60*1:
			print('refreshing news')
			start = time.time()
			sources = get_sources(dynamodb)
			add_feeds(sources)
			for i in sources:
				if 'link' in i:
					dlthreads.urlq.put_nowait(i['link']['S'])

		try:
			rr = dlthreads.urlr.get_nowait()
		except Empty:
			pass
		else:
			res_url, result = rr[0], rr[1]
			if (result and result != 'failed'):
				new_data[res_url] = result

		for i in range(len(newsfeeds)):
			newsfeeds[i].get_news(refresh=True)
			if (newsfeeds[i].get_unread() > 0):
				content = newsfeeds[i].read()
				for c in content:
					c.print_values()
					if (c.is_valid()):
						saveNewsEntry(c, dynamodb)
					else:
						print('Notícia acima não é valida!!!')
					


if __name__ == '__main__':
	main()
