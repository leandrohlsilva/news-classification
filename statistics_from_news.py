import feedparser
import rssfinder
import os.path
import re
from bayesian import classify, classify_file

TAG_RE = re.compile(r'<[^>]+>') #constante usada para remover tags

def clean_html_text(text):
	return TAG_RE.sub('', text).strip(' ').replace('\n',' ')

def add_words_to_dictionary(news_dictionary, line):
	for word in line.strip(',.').split(' '):
		if (word not in news_dictionary):
			news_dictionary[word] = 0
		news_dictionary[word] = news_dictionary[word] + 1

def add_to_dictionary(news_dictionary, entry):
	#for key, value in entry.items():
	add_words_to_dictionary(news_dictionary, entry.title)
	#add_words_to_dictionary(news_dictionary, entry.summary)
	#add_words_to_dictionary(news_dictionary, entry.summary_detail)

def add_to_set(entry, newsSet):
	newsSet.add(clean_html_text(entry.title) + ". " + clean_html_text(entry.summary) + '\n')


def readFromFile(filename, newsSet):
	if (not os.path.isfile(filename)):
		backupfile = open(filename,'a')
		backupfile.close()
	backupfile = open(filename,'r')
	listOfNews = backupfile.readlines()
	for news in listOfNews:
		newsSet.add(news)
	backupfile.close()

def saveToFile(filename, newsSet):
	backupfile = open(filename,'w')
	for item in newsSet:
		backupfile.write(item)
	backupfile.close()

newsSet = set()

filename = "noticias.txt"

readFromFile(filename, newsSet)

fontes_noticias = set()

readFromFile('fonte_noticias.txt', fontes_noticias)

for fonte in fontes_noticias:
	print(fonte)
	rssfeeds = rssfinder.getFeeds(fonte.strip(' \n'))
	if (len(rssfeeds) > 0):
		document = feedparser.parse(rssfeeds[0])
		for entry in document.entries:
			add_to_set(entry, newsSet)
	else:
		print("Fonte " + fonte + " não tem RSS disponivel")

print('Salvando noticias novas ao arquivo')
saveToFile(filename, newsSet)


print("\n\n\nNoticias de interesse:\n")
true_positives = set()
readFromFile("true_positives.txt", true_positives)
print(true_positives)

print("\n\n\nNoticias nao interessantes:\n")
true_negatives = set()
readFromFile("true_negatives.txt", true_negatives)
print(true_negatives)

print('Fazendo a classificacao de todas as noticias encontradas\n\n')
for news in newsSet:
	print(news.strip('\n') + " -> " + classify(news, {'FIFA': true_positives, 'NO': true_negatives}))