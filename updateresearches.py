#!/usr/bin/env python

"""
NewsFeed

"""

__author__    = "Leandro Lourenco (leandro@parallaxis.com.br)"

import sys
assert sys.version >= '3.3', "This program does not work with older versions of Python.\
 Please install Python 3.3 or later."

import os, sys, time, string, re, webbrowser, pickle, signal, socket, urllib.request, urllib.error, urllib.parse, difflib
socket.setdefaulttimeout(20)
from hashlib import md5
from multiprocessing import Queue
from queue import Empty, Full
from html.entities import html5 as entdic
from html.parser import HTMLParser
import xmltodict
from boto3 import session
import xml
from bayesian import Bayes, classify

dynamodb = session.connect_to('dynamodb', region_name='sa-east-1')

def simplify_text(text):

	#remove punctuation
	text = text.translate(text.maketrans('', '', string.punctuation))

	#remove digits
	text = text.translate(text.maketrans('', '', string.digits))

	#convert to lower case
	text = text.lower()

	UNNEEDED_WORDS = ["a", "e", "ou", "à", "de", "do", "em", "até", "no", "que", "enquanto", "isso", "por", "sobre"]
	text = re.sub('\s+(a|e|o|ou|à|da|de|do|em|até|no|que|enquanto|isso|por|sobre)\s+', ' ', text)

	#remove 1 letter words
	text = re.sub('\s+(\w{1})\s+', ' ', text)

	return text



def get_researches():
	result = dynamodb.scan(table_name='news_source', scan_filter={'type': {'AttributeValueList': [{'S':'Research'}], 'ComparisonOperator':'EQ'}})
	return result['Items']

def update_researches_model(researches):
	models = {}
	for research in researches:
		instances = {}
		#load all cases
		cases = dynamodb.query(table_name='news_classified', index_name='source-date-index', key_conditions={'source': {'AttributeValueList': [{'S': research['name']['S']}], 'ComparisonOperator': 'EQ'}}, scan_index_forward=False)
		cases = cases['Items']
		for c in cases:
			if 'real_label' in c:
				c['title']['S'] = simplify_text(c['title']['S'])
				c['description']['S'] = simplify_text(c['description']['S'])
				instances[c['real_label']['S']] = instances.get(c['real_label']['S'], [])
				instances[c['real_label']['S']].append(c['title']['S'] + ' ' + c['description']['S'])
		# Use str.split to extract features/events/words from the corpus and build
		# the model.
		if (instances):
			model = Bayes.extract_events_odds(instances, str.split)
			research['model'] = model
			research['instances'] = instances

		for key in instances:
			print("{} Instances size: {}".format(key, len(instances[key])))
		"""
		All classes have same prior probability. It might be changed in the future for better tunning.
		"""
		#research['bayes'] = Bayes({class_: len(research['instances'][class_]) for class_ in research['instances']})

"""
Still need to set Query_Filter to remove classified news
"""
def get_unclassfied_news(sources):
	unclassified = []
	for s in sources:
		u = dynamodb.query(table_name='news', index_name='source-date-index', key_conditions={'source': {'AttributeValueList': [{'S': s}], 'ComparisonOperator': 'EQ'}}, scan_index_forward=False)
		unclassified.extend(u['Items'])
	return unclassified

def duplicate_entry_save_predictions(case):
	dynamodb.put_item(table_name='news', item=case)
	dynamodb.put_item(table_name='news_classified', item=case)

def flag_as_seen(case, researchName):
	case['classified_by']['SS'].append(researchName)
	dynamodb.update_item(table_name='news', key={'link': case['link']}, AttributeUpdates={'classified_by': case['classified_by']})

def classify_news(unclassified, research):
	if (research.get('model')):
		for case in unclassified:
			original_case = case.copy()
			original_link = case['link']['S']
			original_case['classified_by'] = original_case.get('classified_by', {'SS': []})
			#Calculate probabilities for only unseen cases
			if research['name']['S'] not in original_case['classified_by']['SS']:
				instance = simplify_text(case['title']['S'] + ' ' + case['description']['S'])
				#instance = simplify_text(case['title']['S'])



				research['bayes'] = classify(instance, research['instances'])

				print(instance)
				most_likely = research['bayes'].most_likely()
				print('{}: {}'.format(most_likely, research['bayes'][most_likely]))

				case['link']['S'] = case['link']['S'] + '?' + research['name']['S']
				case['source']['S'] = research['name']['S']
				case['pred_label'] = {'S': most_likely}
				case['pred_prob'] = {'N': str(research['bayes'][most_likely])}
				duplicate_entry_save_predictions(case)
				original_case['link']['S'] = original_link
				flag_as_seen(original_case, research['name']['S'])
			#print(classify(instance, research['instances']))

def main():
	
	start = time.time()

	researches = get_researches()
	update_researches_model(researches)
	

	while (True):
		print('classifying news')

		for research in researches:
			unclassifiedNews = get_unclassfied_news(research['sources']['SS'])
			#print(unclassifiedNews)
			if unclassifiedNews:
				classify_news(unclassifiedNews, research)

		time.sleep(60)

		#Update all researches model every 60*30 seconds
		if (time.time() - start > 60*1):
			update_researches_model(researches)
			


if __name__ == '__main__':
	main()
